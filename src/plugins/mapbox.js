import Vue from 'vue';
import Mapbox from 'mapbox-gl'
import VueMapbox from "vue-mapbox";

Vue.use(VueMapbox, { mapboxgl: Mapbox });