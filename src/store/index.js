import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//Store qui centralise notre état pour le partagé entre les différents composants de notre application

export default new Vuex.Store({
  state: {
      tasks: []
  },
  mutations: {
      setTasks(state, data) {
          state.tasks = data
      },
      addTask(state, task) {
          state.tasks.push(task)
      },
      deleteTask(state, taskId) {
          state.tasks = state.tasks.filter(el => el.id !== taskId);
      },
      updateTask(state, task) {
          state.tasks[state.tasks.findIndex(el => el.id == task.id)] = task;
      }
  },
  actions: {},
})